
import Node from '../src';

import fshader from "./fshader.glsl";

export default ({ container }) => {
    return (args) => Node({ container, fshader, stats: true, mouse: true, ...args, })
};