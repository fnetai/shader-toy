export default function (context) {
  const { canvas, gl } = context;
  const { container } = context.args;

  const resizeCanvas = () => {
    canvas.width = container.clientWidth;
    canvas.height = container.clientHeight;
    gl.viewport(0, 0, canvas.width, canvas.height);
  };

  window.addEventListener('resize', resizeCanvas);

  resizeCanvas();

  return {
    resizeCanvas,
  }
}