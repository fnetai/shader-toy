export default function (context) {
    const {
        canvas,
        gl,
        vertexBuffer,
        vertices,
        shaderProgram,
        mouseInput,
        mouseEnabled,
        statsEnabled,
        stats,
    } = context;

    let lastTime = performance.now();
    let frameCount = 0;

    // Setup code
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.useProgram(shaderProgram);

    const iTime = gl.getUniformLocation(shaderProgram, 'iTime');
    const iResolution = gl.getUniformLocation(shaderProgram, 'iResolution');
    const iTimeDelta = gl.getUniformLocation(shaderProgram, 'iTimeDelta');
    const iFrame = gl.getUniformLocation(shaderProgram, 'iFrame');
    const iFrameRate = gl.getUniformLocation(shaderProgram, 'iFrameRate');
    const iMouse = gl.getUniformLocation(shaderProgram, 'iMouse');

    const position = gl.getAttribLocation(shaderProgram, 'aVertexPosition');
    gl.enableVertexAttribArray(position);
    gl.vertexAttribPointer(position, 2, gl.FLOAT, false, 0, 0);
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);

    const animate = () => {

        statsEnabled && stats.begin();
        
        const currentTime = performance.now();
        const timeDelta = (currentTime - lastTime) / 1000.0; // in seconds
        const frameRate = 1.0 / timeDelta; // frames per second

        gl.uniform1f(iTime, performance.now() / 1000.0);
        gl.uniform3f(iResolution, canvas.width, canvas.height, canvas.width / canvas.height);
        mouseEnabled && gl.uniform4f(iMouse, ...mouseInput);

        iTimeDelta !== null && gl.uniform1f(iTimeDelta, timeDelta);
        iFrameRate !== null && gl.uniform1f(iFrameRate, frameRate);
        iFrame !== null && gl.uniform1i(iFrame, frameCount);

        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, vertices.length / 2);

        statsEnabled && stats.end();
        requestAnimationFrame(animate);
    };

    animate();

    return {
        animate,
    }
}