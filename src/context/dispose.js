export default function (context) {

    const {
        statsEnabled,
        stats,
        container,
        canvas,
        gl,
        vertexBuffer,
        vertexShader,
        fragmentShader,
        shaderProgram,
        resizeCanvas,
    } = context;

    window.removeEventListener('resize', resizeCanvas);
    statsEnabled && stats.dispose();
    container.removeChild(canvas);
    gl.deleteBuffer(vertexBuffer);
    gl.deleteShader(vertexShader);
    gl.deleteShader(fragmentShader);
    gl.deleteProgram(shaderProgram);
}