import fshaderDefault from "../fshader.glsl";

export default function (context) {
    const { gl } = context;
    const {fshader, vshader} = context.args;
    
    let glslVersion;
    let vsSource;
    let fsSource;

    if (gl instanceof WebGL2RenderingContext) {
        glslVersion = `#version 300 es`;
        vsSource = vshader || `${glslVersion}
in vec4 aVertexPosition;
void main(void) {
    gl_Position = aVertexPosition;
}`;
        fsSource = `${glslVersion}
precision mediump float;
uniform float iTime;
uniform vec3 iResolution;
uniform float iTimeDelta;
uniform float iFrameRate;
uniform int iFrame;
uniform vec4 iMouse;
out vec4 fragColor;
${fshader || fshaderDefault}
void main(void) {
    mainImage(fragColor, gl_FragCoord.xy);
}`;
    } else {
        glslVersion = '';
        vsSource = vshader || `
attribute vec4 aVertexPosition;
void main(void) {
    gl_Position = aVertexPosition;
}`;
        fsSource = `
precision mediump float;
uniform float iTime;
uniform vec3 iResolution;
uniform float iTimeDelta;
uniform float iFrameRate;
uniform int iFrame;
uniform vec4 iMouse;
${fshader || fshaderDefault}
void main(void) {
    mainImage(gl_FragColor, gl_FragCoord.xy);
}`;
    }

    context.vsSource = vsSource;
    context.fsSource = fsSource;

    return {
        vsSource,
        fsSource,
    }
}