export default function (context) {
    const { container, canvas: canvasOptions } = context.args;

    const canvas = document.createElement('canvas');
    canvas.width = container.clientWidth;
    canvas.height = container.clientHeight;
    canvas.style.backgroundColor = canvasOptions?.backgroundColor || '#000000';
    canvas.style.overflowClipMargin = "content-box";
    canvas.style.overflow = "clip";
    canvas.style.position = "absolute";
    canvas.style.top = "0";
    canvas.style.left = "0";
    canvas.style.width = "100%";
    canvas.style.height = "100%";
    container.style.position = "relative";
    
    container.appendChild(canvas);

    let gl = canvas.getContext('webgl2');

    if (!gl) {
        console.log('WebGL 2 not supported, falling back on WebGL 1');
        gl = canvas.getContext('webgl');
    }

    if (!gl) {
        console.log('WebGL not supported, falling back on experimental-webgl');
        gl = canvas.getContext('experimental-webgl');
    }

    if (!gl) {
        throw new Error('Your browser does not support WebGL');
    }

    context.gl = gl;
    context.canvas = canvas;

    return {
        canvas,
        gl,
    }
}