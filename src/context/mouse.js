export default function (context) {

    const { canvas } = context;
    const { mouse: mouseOptions } = context.args;

    let mouseInput = null;
    let mouseEnabled = mouseOptions && mouseOptions?.enabled !== false;
    if (!mouseEnabled) return { enabled: false };

    mouseInput = [0, 0, 0, 0];

    canvas.addEventListener('mousemove', function (event) {
        mouseInput[0] = event.clientX;
        mouseInput[1] = canvas.height - event.clientY;
    });

    canvas.addEventListener('mousedown', function (event) {
        mouseInput[2] = event.clientX;
        mouseInput[3] = canvas.height - event.clientY;
    });

    canvas.addEventListener('mouseup', function () {
        mouseInput[2] = 0;
        mouseInput[3] = 0;
    });

    context.mouseInput = mouseInput;
    context.mouseEnabled = mouseEnabled;

    return {
        enabled: true,
        mouseInput,
    }
}