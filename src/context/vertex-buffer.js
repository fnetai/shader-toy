export default function (context) {
    const { gl } = context;

    const vertices = new Float32Array([
        -1.0, -1.0,
        1.0, -1.0,
        -1.0, 1.0,
        1.0, 1.0,
    ]);

    const vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    context.vertices = vertices;
    context.vertexBuffer = vertexBuffer;
    
    return {
        vertices,
        vertexBuffer,
    }
}