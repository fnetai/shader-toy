import Stats from 'stats.js';

export default function (context) {
    const { container, stats: statsOptions } = context.args;

    let statsEnabled = statsOptions && statsOptions?.enabled !== false;
    if (!statsEnabled)
        return {
            enabled: false
        };

    const stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '12px';
    stats.domElement.style.left = '12px';
    stats.domElement.style.zIndex = 1000;
    container.appendChild(stats.domElement);

    stats.dispose = () => {
        container.removeChild(stats.domElement);
    }

    context.stats = stats;
    context.statsEnabled = statsEnabled;

    return {
        enabled: true,
        stats,
    }
}