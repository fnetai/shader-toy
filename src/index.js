import createStats from './context/stats.js';
import createCanvas from "./context/canvas.js";
import createMouse from "./context/mouse.js";
import createShaderSource from "./context/shader-source.js";
import createProgram from "./context/program.js";
import createVertextBuffer from "./context/vertex-buffer.js";
import dispose from "./context/dispose.js";
import handleResize from "./context/handle-resize.js";
import handleAnimate from "./context/handle-animate.js";

export default (args) => {

    const context = {
        api: {},
        args,
    }

    createStats(context);
    createCanvas(context);
    createMouse(context);
    createShaderSource(context);
    createProgram(context);
    createVertextBuffer(context);
    handleResize(context);
    handleAnimate(context);

    return () => dispose(context);
};