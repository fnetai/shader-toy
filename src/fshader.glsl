void mainImage(out vec4 fragColor,in vec2 fragCoord)
{
    vec2 p=(2.*fragCoord.xy-iResolution.xy)/iResolution.y;
    
    fragColor=vec4(5./(40.*abs(2.*length(p)-1.)));
}