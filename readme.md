# @fnet/shader-toy

## Introduction

@fnet/shader-toy is a simple and straightforward library designed for users interested in experimenting with shader programming using WebGL. This project provides an environment where you can create, compile, and display shader effects directly in the browser. It's an accessible tool for those who want to explore computer graphics, enabling easy integration of shaders into web applications.

## How It Works

The library operates by setting up a WebGL context within a canvas element, compiling the user's vertex and fragment shaders, and rendering the output using the Graphics Processor Unit (GPU). It supports WebGL 2, and gracefully falls back to earlier versions if needed. The project also offers mouse interaction capabilities, allowing for dynamic and interactive visual effects.

## Key Features

- **Shader Compilation and Rendering**: Compiles user-defined vertex and fragment shaders, providing real-time visual feedback on the canvas.
- **WebGL Context Management**: Handles the setup and management of a WebGL context, ensuring compatibility and seamless operation across different browsers.
- **Mouse Interaction**: Offers mouse input handling to interact with the shader environment, enabling dynamic effects based on user input.
- **Performance Monitoring**: Integrates with performance stats to provide insights into rendering performance, useful for optimization and learning.
- **Responsive Design**: Adjusts canvas rendering to the size of the container, ensuring consistent display across different device sizes.

## Conclusion

@fnet/shader-toy is a useful tool for developers and enthusiasts interested in shader programming with WebGL. It simplifies the process of creating interactive graphics, offering a starting point for further exploration in web-based GPU programming. The project is designed to be easy to use, making it accessible for experimentation and learning.