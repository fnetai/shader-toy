# Developer Guide for @fnet/shader-toy

## Overview

The `@fnet/shader-toy` library is designed for developers who wish to integrate and render GLSL shaders directly in their web applications. It provides an easy-to-use interface for setting up a WebGL context, compiling and utilizing vertex and fragment shaders, and handling real-time animation and interactivity. The library aims to streamline the creation of shader-based animations or visual effects, allowing developers to focus on shader code without handling the intricate setup of WebGL rendering from scratch.

## Installation

To install `@fnet/shader-toy`, use either npm or yarn. This will automatically handle the installation of any required dependencies.

Using npm:
```bash
npm install @fnet/shader-toy
```

Using yarn:
```bash
yarn add @fnet/shader-toy
```

## Usage

To use the library, you will create a container element in your HTML where the canvas will render the shader. You then call the default export with the required configuration parameters, including shaders and input settings.

Here is a basic example demonstrating the setup and use of the shader-toy library:

### Example Setup

In your HTML:

```html
<div id="shader-container" style="width: 800px; height: 600px;"></div>
```

In your JavaScript:

```javascript
import createShaderApp from '@fnet/shader-toy';

const container = document.getElementById('shader-container');

const dispose = createShaderApp({
    container,
    canvas: { backgroundColor: '#000000' },
    mouse: { enabled: true },
    stats: { enabled: true },
    fshader: `
        void mainImage(out vec4 fragColor, in vec2 fragCoord) {
            vec2 uv = fragCoord.xy / iResolution.xy;
            fragColor = vec4(uv.x, uv.y, 0.5 + 0.5 * sin(iTime), 1.0);
        }
    `,
});

// When you need to stop and cleanup (e.g. on component unmount in React)
dispose();
```

This setup initializes a shader rendering in a specific HTML element and allows interactivity with mouse movements.

### Custom Shaders

You can provide your own GLSL shaders in the configuration object to render custom effects. The library handles GLSL versioning, compiling shaders, and managing shader programs.

## Examples

### Basic Shader with Mouse Interaction

```javascript
import createShaderApp from '@fnet/shader-toy';

const container = document.getElementById('shader-container');

const dispose = createShaderApp({
    container,
    fshader: `
        void mainImage(out vec4 fragColor, in vec2 fragCoord) {
            vec2 p = (2.0 * fragCoord.xy - iResolution.xy) / iResolution.y;
            float d = length(p - (iMouse.xy / iResolution.xy) + 0.5);
            fragColor = vec4(vec3(d), 1.0);
        }
    `,
    mouse: { enabled: true },
});
```

### Animated Shader Example

```javascript
import createShaderApp from '@fnet/shader-toy';

const container = document.getElementById('shader-container');

createShaderApp({
    container,
    fshader: `
        void mainImage(out vec4 fragColor, in vec2 fragCoord) {
            float time = iTime;
            vec2 uv = fragCoord.xy / iResolution.xy;
            vec3 col = 0.5 + 0.5 * cos(time + uv.xyx + vec3(0, 2, 4));
            fragColor = vec4(col, 1.0);
        }
    `,
});
```

These examples demonstrate simple usages for creating interactive and animated visuals using the library. Modify the `fshader` parameter to try out different GLSL fragment shader effects.

## Acknowledgement

This library utilizes `stats.js` for performance monitoring, allowing developers to evaluate the rendering performance of their shaders in real time.